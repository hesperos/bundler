# bundler

**bundler** is a self extracting archive builder.

## Usage

1. Place your payload into payload directory
2. Create build directory
3. Configure & install the project - this will create a self extracting
   script/archive.

## Example

Having an application tree:

```
.
├── bin
│   ├── bar
│   ├── baz
│   └── foo
├── lib
│   ├── libbar.so
│   ├── libbaz.so
│   └── libfoo.so
└── share
    ├── bar.png
    ├── baz.png
    └── foo.pn
```

... that should be installed under `/usr/local`, the package preparation
should be done the following way:

    cd bundler
    cp -r <application tree>/* payload
    mkdir build && cd build
    cmake -DCMAKE_INSTALL_PREFIX=/usr/local ..
    make && make install
    ls -l bin

    ```
    total 12
    -rwxr-xr-x 1 tomasz users 12212 05-16 16:19 bundler
    ```

The bundle is ready to use in build/bin directory!
